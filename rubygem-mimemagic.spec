%global gem_name mimemagic

Name: rubygem-%{gem_name}
Version: 0.3.1
Release: 1%{?dist}
Summary: Fast mime detection by extension or content
Group: Development/Languages
License: MIT
URL: https://github.com/minad/mimemagic
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: rubygem(bacon)
BuildArch: noarch

%description
Fast mime detection by extension or content in pure ruby (Uses
freedesktop.org.xml shared-mime-info database).


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

for f in .gitignore .travis.yml .yardopts Gemfile Rakefile script/*; do
  rm $f
  sed -i "s|\"$f\",||g" %{gem_name}.gemspec
done

%build
gem build %{gem_name}.gemspec
%gem_install
# remove unnecessary gemspec
rm .%{gem_instdir}/%{gem_name}.gemspec

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
       %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
  ruby -Ilib test/*_test.rb
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/LICENSE
%doc %{gem_instdir}/README.md
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}
%exclude %{gem_instdir}/test

%files doc
%doc %{gem_docdir}

%changelog
* Fri Jan 08 2016 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.3.1-1
- Initial package
